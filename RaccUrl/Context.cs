﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Microsoft.Win32;

namespace RaccUrl
{
    internal sealed class Context : ApplicationContext, IMessageFilter
    {
        NotifyIcon _icon;
        RegistryKey _startupKey;
        RegistryKey _settingsKey;
        MenuItem _selectShortener;
        bool _shouldClipboardMonitorContinue;

        public Context()
        {
            _startupKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            _settingsKey = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\MysteryDash\RaccUrl");

            _selectShortener = new MenuItem(Localization.strings.SelectShortener);
            bool isShortenerSelected = false;
            foreach (string shortener in UrlShorteners.Keys)
            {
                _selectShortener.MenuItems.Add(new MenuItem(shortener, (s, e) => {
                    var sender = (MenuItem)s;
                    foreach (MenuItem item in _selectShortener.MenuItems)
                    {
                        item.Checked = false;
                    }
                    sender.Checked = true;

                    _settingsKey.SetValue("Shortener", sender.Text);
                }) { RadioCheck = true, Checked = (string)_settingsKey.GetValue("Shortener", "") == shortener });
                isShortenerSelected |= (string)_settingsKey.GetValue("Shortener", "") == shortener;
            }
            if (!isShortenerSelected)
                _selectShortener.MenuItems[0].PerformClick();
            _icon = new NotifyIcon()
            {
                Icon = Properties.Resources.Cut,
                Visible = true,
                Text = "RaccUrl",
                ContextMenu = new ContextMenu(new MenuItem[] { 
                    new MenuItem(Localization.strings.ClipboardMonitor, SwitchMonitor) { Checked = true },
                    new MenuItem(Localization.strings.ShowNotifications, SwitchNotifs) {Checked = Convert.ToBoolean(_settingsKey.GetValue("Notifications", 1)) },
                    new MenuItem(Localization.strings.StartWithWindows, SwitchStartup) { Checked = (string)_startupKey.GetValue("RaccUrl", "") == Application.ExecutablePath },
                    new MenuItem("-"),
                    _selectShortener,
                    new MenuItem("-"),
                    new MenuItem(Localization.strings.Exit, Exit),
                }),
            };

            Application.AddMessageFilter(this);
            StartMonitor();
        }

        void SwitchMonitor(object sender, EventArgs e)
        {
            var switchMonitorMenuItem = (MenuItem)sender;
            if (switchMonitorMenuItem.Checked)
            {
                _shouldClipboardMonitorContinue = false;
            }
            else
            {
                StartMonitor();
            }

            switchMonitorMenuItem.Checked = !switchMonitorMenuItem.Checked;
        }

        void StartMonitor()
        {
            if (_shouldClipboardMonitorContinue == false)
            {
                var thread = new Thread(MonitorClipboard);
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }

        void MonitorClipboard()
        {
            _shouldClipboardMonitorContinue = true;
            int lastHashCode = 0;
            
            while (_shouldClipboardMonitorContinue)
            {
                if (Clipboard.ContainsText())
                {
                    if (lastHashCode != Clipboard.GetText().GetHashCode())
                    {
                        int updatedUrls = 0;
                        string shortener = "";
                        foreach (MenuItem item in _selectShortener.MenuItems)
                        {
                            if (item.Checked)
                                shortener = item.Text;
                        }
                        var updatedClipboardContent = Regex.Replace(Clipboard.GetText(),
                            @"(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'"" .,<>?«»“”‘’]))",
                            new MatchEvaluator((url) => {
                                string urlShortened;
                                if (url.Value.Length > 24 && UrlShorteners.GetShortener(shortener).ShortenUrl(url.Value, out urlShortened))
                                {
                                    updatedUrls++;
                                    return urlShortened; 
                                }
                                else
                                {
                                    return url.Value;
                                }
                            }));

                        if (updatedUrls > 0)
                        {
                            Clipboard.SetText(updatedClipboardContent);
                            if (Convert.ToBoolean(_settingsKey.GetValue("Notifications", 1)))
                                _icon.ShowBalloonTip(2500, Localization.strings.ClipboardUpdated, String.Format(Localization.strings.ShortenedXUrls, updatedUrls.ToString()), ToolTipIcon.Info);
                            lastHashCode = Clipboard.GetText().GetHashCode();
                        }
                    }
                }
                Thread.Sleep(500);
            }
        }

        void SwitchNotifs(object sender, EventArgs e)
        {
            var switchNotifsMenuItem = (MenuItem)sender;
            if (switchNotifsMenuItem.Checked)
                _settingsKey.SetValue("Notifications", 0, RegistryValueKind.DWord);
            else
                _settingsKey.SetValue("Notifications", 1, RegistryValueKind.DWord);

            switchNotifsMenuItem.Checked = !switchNotifsMenuItem.Checked;
        }

        void SwitchStartup(object sender, EventArgs e)
        {
            var switchStartupMenuItem = (MenuItem)sender;
            if (switchStartupMenuItem.Checked)
                _startupKey.DeleteValue("RaccUrl", false);
            else
                _startupKey.SetValue("RaccUrl", Application.ExecutablePath, RegistryValueKind.String);

            switchStartupMenuItem.Checked = !switchStartupMenuItem.Checked;
        }

        public bool PreFilterMessage(ref Message m)
        {
            if (m.Msg == NativeMethods.WM_ALREADYRUNNING)
            {
                _icon.ShowBalloonTip(3000, Localization.strings.AlreadyRunning, Localization.strings.OneInstanceOnly, ToolTipIcon.Warning);
                return true;
            }
            return false;
        }

        void Exit(object sender, EventArgs e)
        {
            _shouldClipboardMonitorContinue = false;
            _icon.Visible = false;
            Application.RemoveMessageFilter(this);

            Application.Exit();
        }
    }
}
