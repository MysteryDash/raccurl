﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Windows.Forms;

namespace RaccUrl
{
    internal static class UrlShorteners
    {
        private static OrderedDictionary _shorteners;

        public static ICollection Keys { get { return _shorteners.Keys; } }

        static UrlShorteners()
        {
            _shorteners = new OrderedDictionary() {
                { "Is.Gd", new IsGd() },
                { "Koinko.in", new Koinko() },
                { "Qr.net", new QrNet() },
                { "Tinyurl", new Tinyurl() },
                { "U.To", new UTo() },
                { "V.Gd", new VGd() },
                { "5.Gp", new FiveGp() },
            };
        }

        public static IUrlShortener GetShortener(string name)
        {
            if (ContainsShortener(name))
                return (IUrlShortener)_shorteners[name];
            return (IUrlShortener)_shorteners[0];
        }

        public static bool ContainsShortener(string name)
        {
            return _shorteners.Contains(name);
        }
    }

    internal interface IUrlShortener
    {
        bool ShortenUrl(string url, out string shortenedUrl);
    }

    internal sealed class IsGd : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";
            using (var client = new WebClient())
            {
                try
                {
                    var response = client.DownloadString("http://is.gd/create.php?format=simple&url=" + HttpUtility.UrlEncode(url));
                    if (response.Contains("Error"))
                    {
                        return false;
                    }
                    else
                    {
                        shortenedUrl = response;
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }
    }

    internal sealed class Koinko : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";

            NameValueCollection values = new NameValueCollection();
            values.Add("url", url);

            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                try
                {
                    var json = new DataContractJsonSerializer(typeof(Response));
                    var response = (Response)json.ReadObject(new MemoryStream(client.UploadValues("http://koinko.in/shorten", values)));
                    if (response.result)
                    {
                        shortenedUrl = response.url;
                        return true;
                    }
                    return false;
                }
                catch
                {
                    return false;
                }
            }
        }

        [DataContract]
        internal sealed class Response
        {
            #pragma warning disable 0649
            [DataMember]
            public bool result;
            [DataMember]
            public string url;
            #pragma warning restore 0649
        }
    }

    internal sealed class QrNet : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";
            using (var client = new WebClient())
            {
                try
                {
                    var json = new DataContractJsonSerializer(typeof(Response));
                    var response = (Response)json.ReadObject(new MemoryStream(client.DownloadData("http://qr.net/api/short?longurl=" + url)));
                    if (response.url == null)
                    {
                        return false; 
                    }
                    shortenedUrl = response.url;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        [DataContract]
        internal sealed class Response
        {
            #pragma warning disable 0649
            [DataMember]
            public string url;
            #pragma warning disable 0649
        }
    }

    internal sealed class Tinyurl : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";

            using (var client = new WebClient())
            {
                try
                {
                    shortenedUrl = client.DownloadString("http://tinyurl.com/api-create.php?url=" + url);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }

    internal sealed class UTo : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";

            NameValueCollection values = new NameValueCollection();
            values.Add("a", "add");
            values.Add("url", HttpUtility.UrlEncode(url));

            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                try
                {
                    Console.WriteLine(Encoding.UTF8.GetString(client.UploadValues("http://u.to/", values)));
                    var match = Regex.Match(Encoding.UTF8.GetString(client.UploadValues("http://u.to/", values)), @"http:\/\/u.to\/[a-zA-Z0-9]+");
                    if (match.Success)
                    {
                        shortenedUrl = match.Value;
                        return true;
                    }
                    return false;
                }
                catch
                {
                    return false;
                }
            }
        }
    }

    internal sealed class VGd : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";
            using (var client = new WebClient())
            {
                try
                {
                    var response = client.DownloadString("http://v.gd/create.php?format=simple&url=" + HttpUtility.UrlEncode(url));
                    if (response.Contains("Error"))
                    {
                        return false;
                    }
                    else
                    {
                        shortenedUrl = response;
                        return true;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }
    }

    internal sealed class FiveGp : IUrlShortener
    {
        public bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";
            using (var client = new WebClient())
            {
                try
                {
                    var json = new DataContractJsonSerializer(typeof(Response));
                    var response = (Response)json.ReadObject(new MemoryStream(client.DownloadData("http://5.gp/api/short?longurl=" + url)));
                    if (response.url == null)
                    {
                        return false;
                    }
                    shortenedUrl = response.url;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        [DataContract]
        internal sealed class Response
        {
            #pragma warning disable 0649
            [DataMember]
            public string url;
            #pragma warning disable 0649
        }
    }
}
