﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace RaccUrl
{
    static class Program
    {
        static Mutex mutex = new Mutex(true, ((GuidAttribute)(Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), true)[0])).Value);
        [STAThread]
        static void Main()
        {
            if (mutex.WaitOne(0, true))
            {
                Application.Run(new Context());
                mutex.ReleaseMutex();
            }
            else
            {
                NativeMethods.PostMessage((IntPtr)0xFFFF, NativeMethods.WM_ALREADYRUNNING, IntPtr.Zero, IntPtr.Zero);
            }
        }
    }
}
