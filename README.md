# RaccUrl #

An open-sourced fully-automated software used to shorten long url(s) stored in the clipboard

### URL shorteners used in this project ###

* [KoinKo.in](http://koinko.in)
* [TinyURL](http://tinyurl.com)
* [And others not mentionned here !]

### License ###

This project is licensed under the terms of the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).